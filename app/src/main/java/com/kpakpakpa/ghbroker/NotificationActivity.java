package com.kpakpakpa.ghbroker;

import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kpakpakpa.ghbroker.data.BrokerContract;

public class NotificationActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private CursorAdapter NotificationAdapter;

    private static final int NOTIFICATION_LOADER = 0;

    public final String[] NOTIFICATION_COLUMNS = {
            BrokerContract.NotificationEntry._ID,
            BrokerContract.NotificationEntry.COLUMN_TYPE,
            BrokerContract.NotificationEntry.COLUMN_CONTENT,
            BrokerContract.NotificationEntry.COLUMN_DATE,
            BrokerContract.NotificationEntry.COLUMN_TIME
    };

    public final String[] cursorInputs = {
            BrokerContract.NotificationEntry.COLUMN_TYPE,
            BrokerContract.NotificationEntry.COLUMN_CONTENT,
            BrokerContract.NotificationEntry.COLUMN_DATE,
            BrokerContract.NotificationEntry.COLUMN_TIME
    };

    public final int[] NOTIFICATION_VIEWS = {
            R.id.type,
            R.id.content,
            R.id.date,
            R.id.time
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NotificationAdapter = new SimpleCursorAdapter(this,R.layout.notification_row,
                                                      null,cursorInputs,NOTIFICATION_VIEWS);

        ListView notificationList  = (ListView) findViewById(R.id.notification_list);
        notificationList.setAdapter(NotificationAdapter);

        notificationList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                showTrackDialog(id);

                return false;
            }
        });

        getSupportLoaderManager().initLoader(NOTIFICATION_LOADER,null,this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.notification_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_delete_all:
                showTrackDialog(-1);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * showTrackDialog function
     *
     * function that shows a dialog with the track status
     * of a currency and gives options to keep tracking or stop
     *
     * @param where
     *
     * @return void
     */
    public void showTrackDialog(long where){
        final Dialog dialog = new Dialog(this);
        final long whereClause = where;

        String titleMessage = (whereClause == -1) ? "Clear All Notifications":
                                                   "Delete This Notification";

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);

        TextView dialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        dialogTitle.setText(titleMessage);

        Button track_status = (Button) dialog.findViewById(R.id.track_status);
        Button dismissDialog = (Button) dialog.findViewById(R.id.dismiss);

        track_status.setText("Delete");

        /**
         * click listener for the track button
         */
        track_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DeleteNotificationTask().execute((int) whereClause);

                dialog.dismiss();
            }
        });

        /**
         * define click listener for the cancel button
         */
        dismissDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        //Toast.makeText(context,cursor.getString( cursor.getColumnIndex("NAME") ),Toast.LENGTH_LONG).show();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, BrokerContract.NotificationEntry.CONTENT_URI,NOTIFICATION_COLUMNS,null,null,"_ID DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        NotificationAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        NotificationAdapter.swapCursor(null);
    }
    /** End of LoaderManager **/


    private class DeleteNotificationTask extends AsyncTask<Integer,Void,Integer>{

        @Override
        protected Integer doInBackground(Integer... params) {
            Integer whereClause = params[0];

            if(whereClause == -1){

                SQLiteDatabase db = GhBrokerOpenHelper.getInstance(NotificationActivity.this).getReadableDatabase();

//                Cursor cursor = db.rawQuery("SELECT * FROM NOTIFICATIONS",null);
//
//                if(cursor.getCount() > 0 && cursor != null) {

                    NotificationActivity.this
                            .getContentResolver()
                            .delete(BrokerContract.NotificationEntry.CONTENT_URI, null, null);

//                }
//
//                cursor.close();
//                db.close();
            }
            else{

                NotificationActivity.this
                        .getContentResolver()
                        .delete(BrokerContract.NotificationEntry.CONTENT_URI,
                                "_ID = ?",new String[]{String.valueOf(whereClause)});

            }

            return 0;
        }
    }
}


