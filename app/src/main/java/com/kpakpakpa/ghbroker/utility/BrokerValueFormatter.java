package com.kpakpakpa.ghbroker.utility;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by Salis on 5/25/2016.
 */
public class BrokerValueFormatter implements ValueFormatter {

    private DecimalFormat mFormat;

    public BrokerValueFormatter() {
        mFormat = new DecimalFormat("###,###,##0.00"); // use two decimals
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

        return mFormat.format(value);
    }
}
