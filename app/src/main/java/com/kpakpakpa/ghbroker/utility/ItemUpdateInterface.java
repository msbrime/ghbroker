package com.kpakpakpa.ghbroker.utility;

/**
 * Created by Salis on 5/31/2016.
 */
public interface ItemUpdateInterface {

    public void updateComplete(int success);
}
