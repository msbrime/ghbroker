package com.kpakpakpa.ghbroker.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Salis on 5/27/2016.
 */
public class Utilities {

    public static class NetworkChecker{

        public static boolean isNetworkAvailable(Context c){

            ConnectivityManager check = (ConnectivityManager)
                    c.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = check.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                return false;
            }

            return true;
        }

    }

}
