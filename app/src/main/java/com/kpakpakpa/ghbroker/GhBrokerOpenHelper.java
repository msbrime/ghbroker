package com.kpakpakpa.ghbroker;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kpakpakpa.ghbroker.models.Currency;
import com.kpakpakpa.ghbroker.models.Stock;

/**
 * Created by Salis on 4/30/2016.
 */
public class GhBrokerOpenHelper extends SQLiteOpenHelper {

    public static String DB_NAME = "ghStocks";
    public static final String IS_POPULATED = "populated";
    private static int DB_VER = 1;
    public static GhBrokerOpenHelper dbInstance = null;
    private Context mContext;

    private Currency[] pbCurrencies =
                new Currency[]{
                        new Currency("Ghanaian Cedi","GHS",R.drawable.cedi),
                        new Currency("US Dollar","USD",R.drawable.dollar),
                        new Currency("Great Britain Pound","GBP",R.drawable.pound),
                        new Currency("Japanese Yen","JPY",R.drawable.yen),
                        new Currency("Euro","EUR",R.drawable.euro),
                        new Currency("Nigerian Naira","NGN",R.drawable.naira),
                        new Currency("Canadian Dollar","CAD",R.drawable.canadian),
                        new Currency("Australian Dollar","AUD",R.drawable.australian),
                        new Currency("South African Rand","ZAR",R.drawable.rand),
                        new Currency("Indian Rupee","INR",R.drawable.rupee),
                        new Currency("Chinese Yuan","CNY",R.drawable.yuan)

                };

    private Stock[] pbStocks =
            new Stock[]{
                    new Stock("AngloGold Ashanti Limited","AADS",R.drawable.stock),
                    new Stock("African Champions Industries Limited","ACI",R.drawable.stock),
                    new Stock("AngloGold Ashanti Limited","AGA",R.drawable.stock),
                    new Stock("Aluworks Limited","ALW",R.drawable.stock),
                    new Stock("Ayrtn Drugs Manufacturing Company Limited","AYRTN",R.drawable.stock),
                    new Stock("Benso Oil Palm Plantation Limited","BOPP",R.drawable.stock),
                    new Stock("CAL Bank Limited Limited","CAL",R.drawable.stock),
                    new Stock("Clydestone Ghana Limited","CLYD",R.drawable.stock),
                    new Stock("Camelot Ghana Limited","CMLT",R.drawable.stock),
                    new Stock("Cocoa Processing Company Limited","CPC",R.drawable.stock),
                    new Stock("Ecobank Ghana Limited","EGH",R.drawable.stock),
                    new Stock("Enterprise Group Limited","EGL",R.drawable.stock),
                    new Stock("Ecobank Transnational Incorporated","ETI",R.drawable.stock),
                    new Stock("Fan Milk Limited","FML",R.drawable.stock),
                    new Stock("GCB Bank Limited","GCB",R.drawable.stock),
                    new Stock("Guiness Ghana Breweries Limited","GGBL",R.drawable.stock),
                    new Stock("NewGold Issuer Limited","GLD",R.drawable.stock),
                    new Stock("Ghana Oil Company Limited","GOIL",R.drawable.stock),
                    new Stock("Ghana Star Resources Limited","GSR",R.drawable.stock),
                    new Stock("Golden Web Limited","GWEB",R.drawable.stock),
                    new Stock("HFC Bank Ghana Limited","HFC",R.drawable.stock),
                    new Stock("Mega African Capital Limited","MAC",R.drawable.stock),
                    new Stock("Mechanical Lloyd Company Limited","MLC",R.drawable.stock),
                    new Stock("Produce Buying Company Limited","PBC",R.drawable.stock),
                    new Stock("Pioneer Kitchenware Limited","PKL",R.drawable.stock),
                    new Stock("PZ Cussons Ghana Limited","PZC",R.drawable.stock),
                    new Stock("Standard Chartered Bank Ghana Limited","SCB",R.drawable.stock),
                    new Stock("Standard Chartered Bank Ghana Limited","SCBPREF",R.drawable.stock),
                    new Stock("SIC Insurance Company","SIC",R.drawable.stock),
                    new Stock("Societe Generale Ghana Limited","SOGEGH",R.drawable.stock),
                    new Stock("Starwin Products Limited","SPL",R.drawable.stock),
                    new Stock("Sam Woode Limited","SWL",R.drawable.stock),
                    new Stock("Trust Bank Limited","TBL",R.drawable.stock),
                    new Stock("Tullow Oil PLC","TLW",R.drawable.stock),
                    new Stock("Total Petroleum Ghana Limited","TOTAL",R.drawable.stock),
                    new Stock("Transol Solutions Ghana Limited","TRANSOL",R.drawable.stock),
                    new Stock("Unilever Ghana Limited","UNIL",R.drawable.stock),
                    new Stock("UT Bank Ghana Limited","UTB",R.drawable.stock)
            };

    public static GhBrokerOpenHelper getInstance(Context context){
        if(dbInstance == null){
            dbInstance = new GhBrokerOpenHelper(context);
        }

        return dbInstance;
    }

   private GhBrokerOpenHelper(Context context){
        super(context, DB_NAME,null,DB_VER);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        SharedPreferences prefs = mContext.getSharedPreferences("com.kpakpakpa.broker",Context.MODE_PRIVATE);

        prefs.edit().putBoolean(IS_POPULATED,false).commit();

        updateMyDatabase(db,0,DB_VER);
    }

    private void insertItem(SQLiteDatabase db,String tableName ,String name, String acronym, int resId){

        ContentValues content = new ContentValues();

        content.put("NAME",name);
        content.put("ACRONYM",acronym);
        content.put("IMAGE_RES_ID",resId);

        db.insert(tableName,null,content);

    }

    private void insertRateItem(SQLiteDatabase db,String tableName, String acronym){
        ContentValues content = new ContentValues();

        content.put("ACRONYM",acronym);

        db.insert(tableName,null,content);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void updateMyDatabase(SQLiteDatabase db,int oldVersion,int newVersion){
        if(oldVersion < 1){
            db.execSQL("CREATE TABLE CURRENCY(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "NAME TEXT," +
                    "ACRONYM TEXT," +
                    "IMAGE_RES_ID INTEGER,"+
                    "TRACKED NUMERIC);");

            db.execSQL("CREATE TABLE RATES(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "ACRONYM TEXT," +
                    "RATE REAL DEFAULT 0.00," +
                    "CHANGE REAL DEFAULT 0.00);");

            db.execSQL("CREATE TABLE STOCKS(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "NAME TEXT," +
                    "ACRONYM TEXT," +
                    "IMAGE_RES_ID INTEGER,"+
                    "TRACKED NUMERIC);");

            db.execSQL("CREATE TABLE STOCK_RATES(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "ACRONYM TEXT," +
                    "PRICE REAL DEFAULT 0.00," +
                    "CHANGE REAL DEFAULT 0.00);");


            db.execSQL("CREATE TABLE STOCK_DETAILS(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "NAME TEXT," +
                    "COMPANY TEXT,"+
                    "BOARD TEXT" +
                    ");");

            db.execSQL("CREATE TABLE NOTIFICATIONS(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "TYPE TEXT," +
                    "CONTENT TEXT,"+
                    "TIME TEXT," +
                    "DATE TEXT" +
                    ");");

            for(Currency currency: pbCurrencies){

                insertItem(db,"CURRENCY",currency.getName(),currency.getAcronym(),currency.getImage());
                insertRateItem(db,"RATES",currency.getAcronym());

            }

            for(Stock stock: pbStocks){

                insertItem(db,"STOCKS",stock.getName(),stock.getAcronym(),stock.getImage());
                insertRateItem(db,"STOCK_RATES",stock.getAcronym());

            }
        }
        if(oldVersion < 2){
        }
    }
}
