package com.kpakpakpa.ghbroker;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.kpakpakpa.ghbroker.data.UpdateService;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPagerActivityFragment extends AppCompatActivity {

    public static final String FROM_NOTIFICATION = "startedFrom";
    public static final String SHOW_TAB = "tabToShow";
    int showTab;

    ViewPager initialViewPager = null;

    public ViewPagerActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        setContentView(R.layout.fragment_view_pager_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        UpdateService.startUpdateService(this);

        initialViewPager = (ViewPager) findViewById(R.id.my_view_pager);

        initialViewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch(position){
                    case 0:
                        return new CurrencyListFragment();
                    case 1:
                        return new StockListFragment();

                    default:
                        return  new CurrencyListFragment();
                }
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch(position){
                    case 0:
                        return "Currency";
                    case 1:
                        return "Stocks";
                    default:
                        return "Currency";
                }
            }

            @Override
            public int getCount() {
                return 2;
            }
        });

        //initialViewPager.setCurrentItem(showTab);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        int startedFrom = intent.getIntExtra(FROM_NOTIFICATION,0);
        showTab = intent.getIntExtra(SHOW_TAB,0);

        if(startedFrom == 0) {
            UpdateService.startUpdateService(this);
        }

        if(initialViewPager != null){
            initialViewPager.setCurrentItem(showTab);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.action_converter:
                Intent intent = new Intent(this,ConverterActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_notifications:
                Intent intent2 = new Intent(this,NotificationActivity.class);
                startActivity(intent2);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
