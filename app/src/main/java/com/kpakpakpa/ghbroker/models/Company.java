package com.kpakpakpa.ghbroker.models;

import org.json.JSONObject;

/**
 * Created by Salis on 5/31/2016.
 */
public class Company {

    public String capital;
    public String address;
    public String email;
    public String facsimile;
    public String industry;
    public String name;
    public String sector;
    public String telephone;
    public String website;
    public double dps;
    public double eps;
    public String abrv;
    public double price;
    public double shares;

    public Company(JSONObject json){
        decompose(json);
    }

    private void decompose(JSONObject json){

        this.capital = json.optString("capital");
        this.address = json.optJSONObject("company").optString("address");
        this.email = json.optJSONObject("company").optString("email");
        this.facsimile = json.optJSONObject("company").optString("facsimile");
        this.industry = json.optJSONObject("company").optString("industry");
        this.name = json.optJSONObject("company").optString("name");
        this.sector = json.optJSONObject("company").optString("sector");
        this.telephone = json.optJSONObject("company").optString("telephone");
        this.website = json.optJSONObject("company").optString("website");

        this.dps = json.optDouble("dps");
        this.eps = json.optDouble("eps");
        this.abrv = json.optString("name");
        this.price = json.optDouble("price");
        this.shares = json.optDouble("shares");

    }
}
