package com.kpakpakpa.ghbroker.models;

/**
 * Created by Salis on 5/24/2016.
 */
public class Rate {

    private int id;
    private double rate;
    private String acronym;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Rate(int id, double rate, String acronym){
        this.id = id;
        this.rate = rate;
        this.acronym = acronym;
    }
}
