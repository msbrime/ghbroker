package com.kpakpakpa.ghbroker.models;

import android.content.ContentValues;
import android.content.Context;

/**
 * Created by Salis on 5/27/2016.
 */
public class CurrencyUpdate {

    private ContentValues values;
    private String where;
    private String[] whereArgs;
    private Context context;

    public ContentValues getValues() {
        return values;
    }

    public void setValues(ContentValues values) {
        this.values = values;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String[] getWhereArgs() {
        return whereArgs;
    }

    public void setWhereArgs(String[] whereArgs) {
        this.whereArgs = whereArgs;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public CurrencyUpdate(Context c, ContentValues v, String w, String[] wa){

        this.values = v;
        this.where = w;
        this.whereArgs = wa;
        this.context = c;

    }


}
