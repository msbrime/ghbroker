package com.kpakpakpa.ghbroker.models;

import android.content.Context;
import android.os.AsyncTask;

import com.kpakpakpa.ghbroker.data.BrokerContract;
import com.kpakpakpa.ghbroker.utility.ItemUpdateInterface;

/**
 * Created by Salis on 5/2/2016.
 */
public class Currency {

    private String name;
    private String acronym;
    private int image;
    private int trackStatus;

    private int id;

    public int getTrackStatus() {
        return trackStatus;
    }

    public void setTrackStatus(int trackStatus) {
        this.trackStatus = trackStatus;
    }

    private double rate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }


    public Currency(String n, String a, int i){
        this.name = n;
        this.acronym = a;
        this.image = i;
    }

    public Currency(String n, String a, int i,int track){
        this.name = n;
        this.acronym = a;
        this.image = i;
        this.trackStatus = track;
    }

    public Currency(String n,String a,int i,int id,double rate){
        this.name = n;
        this.acronym = a;
        this.image = i;
        this.id = id;
        this.rate = rate;
    }

    public static class UpdateCurrencyTask extends AsyncTask<CurrencyUpdate,Void,Integer>{

        public ItemUpdateInterface itemUpdate = null;
        private Context context;

        public UpdateCurrencyTask(ItemUpdateInterface iface){
            this.itemUpdate = iface;
        }

        public UpdateCurrencyTask(){
        }

        @Override
        protected Integer doInBackground(CurrencyUpdate... params) {

            CurrencyUpdate currencyUpdate = params[0];

            context = currencyUpdate.getContext();

            int success =currencyUpdate.getContext()
                                        .getContentResolver()
                                        .update(BrokerContract.CurrencyEntry.CONTENT_URI,
                                                currencyUpdate.getValues(),
                                                currencyUpdate.getWhere(),
                                                currencyUpdate.getWhereArgs());

            return success;

        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if(itemUpdate != null) {
                itemUpdate.updateComplete(integer);
            }
        }
    }


}
