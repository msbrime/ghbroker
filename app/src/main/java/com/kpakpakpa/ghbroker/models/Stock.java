package com.kpakpakpa.ghbroker.models;

import android.content.Context;
import android.os.AsyncTask;

import com.kpakpakpa.ghbroker.data.BrokerContract;
import com.kpakpakpa.ghbroker.utility.ItemUpdateInterface;

/**
 * Created by Salis on 5/15/2016.
 */
public class Stock extends Currency {

    public Stock(String n, String a, int i) {
        super(n, a, i);
    }

    public Stock(String n,String a,int i,int id,double rate){
        super(n,a,i,id,rate);
    }

    public Stock(String n, String a, int i,int track){
        super(n,a,i,track);
    }

    public static class UpdateStockTask extends AsyncTask<CurrencyUpdate,Void,Integer> {

        public ItemUpdateInterface itemUpdate = null;
        private Context context;

        public UpdateStockTask(ItemUpdateInterface iface){
            this.itemUpdate = iface;
        }

        public UpdateStockTask(){
        }

        @Override
        protected Integer doInBackground(CurrencyUpdate... params) {

            CurrencyUpdate currencyUpdate = params[0];

            context = currencyUpdate.getContext();

            int success =currencyUpdate.getContext()
                    .getContentResolver()
                    .update(BrokerContract.StockEntry.CONTENT_URI,
                            currencyUpdate.getValues(),
                            currencyUpdate.getWhere(),
                            currencyUpdate.getWhereArgs());

            return success;

        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if(itemUpdate != null) {
                itemUpdate.updateComplete(integer);
            }

        }
    }

}
