package com.kpakpakpa.ghbroker.models;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.kpakpakpa.ghbroker.R;

/**
 * Created by Salis on 5/29/2016.
 */
public class BrokerNotification {

    public static final int NOTIFY_ICON = R.drawable.cedi_white;
    public static final String NOTIFY_TITLE =  "Pocket Broker";
    public static final String NOTIFY_CURRENCY_TITLE = "Currency Update";
    public static final String NOTIFY_STOCK_TITLE = "Stock Update";
    public static final String CURRENCY_TICKER = "Currency Update";

    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private String body;

    private String title = NOTIFY_TITLE;

    private String ticker;

    private Intent intent;

    private int uniqueId;

    private PendingIntent pendingIntent = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public PendingIntent getPendingIntent() {
        return pendingIntent;
    }

    public void setPendingIntent(PendingIntent pendingIntent) {
        this.pendingIntent = pendingIntent;
    }

    public BrokerNotification(Context context, String body, String ticker, Class<?> cls){

        this.context = context;
        this.body = body;
        this.intent= new Intent(context,cls);
        this.ticker = ticker;
        this.uniqueId = (int) (System.currentTimeMillis() & 0xfffffff);

    }

    public void createPendingIntent(){

        this.pendingIntent = PendingIntent
                                .getActivity(
                                             this.context,this.uniqueId,
                                             this.intent,
                                             PendingIntent.FLAG_UPDATE_CURRENT);


    }

    public static void push(BrokerNotification notification){

        NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(notification.getContext());


        builder.setSmallIcon(BrokerNotification.NOTIFY_ICON);
        builder.setAutoCancel(true);
        builder.setContentTitle(notification.getTitle());
        builder.setContentText(notification.getBody());
        builder.setTicker(notification.getTicker());
        builder.setWhen(System.currentTimeMillis());
        builder.setCategory(NotificationCompat.CATEGORY_SYSTEM);
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

       if(notification.pendingIntent != null){
            builder.setContentIntent(notification.pendingIntent);
        }

        NotificationManager notificationManager =
                (NotificationManager) notification.getContext()
                                        .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notification.getUniqueId(), builder.build());


    }


}
