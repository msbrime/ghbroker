package com.kpakpakpa.ghbroker.models;

/**
 * Created by Salis on 5/24/2016.
 */
public class DoubleParts {

    private int integerPart;

    private double decimalPart;

    public double formattedDecimal;

    public String integerPartAsString;

    public String decimalPartAsString;

    public DoubleParts(Double fullDouble){
        this.decompose(fullDouble);
    }

    private void decompose(Double fullDouble){

        this.integerPart = fullDouble.intValue();
        this.integerPartAsString = String.valueOf(this.integerPart);
        this.decimalPart = fullDouble - this.integerPart;
        this.decimalPartAsString = String.format("%.2f",decimalPart).replace("-","");
        this.decimalPartAsString = decimalPartAsString.substring(1);
        this.formattedDecimal = Double.parseDouble(this.integerPartAsString
                                                    + this.decimalPartAsString);

    }

}
