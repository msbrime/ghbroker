package com.kpakpakpa.ghbroker.data;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Salis on 5/2/2016.
 */
public class ApiConnector {

    private URL url;
    private HttpURLConnection urlConnection;

    public ApiConnector(String s) throws MalformedURLException {
        this.url = new URL(s);
    }

    private String getJSONData(){

        String JSONString = "";
        String data = "";

        /**
         * Try to connect to the api url and retrieve rate data
         */
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader =new BufferedReader(new InputStreamReader(in, "UTF-8"));

            while ((data = reader.readLine()) != null){
                JSONString += data;
            }

        }
        catch(IOException e){

            e.printStackTrace();

        }
        finally {
            /** close the connection **/
            urlConnection.disconnect();
        }

        return JSONString;
    }


    public String getJSONStringOutput(){

        return getJSONData();

    }

    public JSONObject getJSONObjectOutput(){

        String JSONString = getJSONData();
        JSONObject jsonObject = null;

        /**
         * Convert the JSON received into a JSONObject
         */
        try {

            jsonObject = new JSONObject(JSONString);

        }
        catch(JSONException jse){
            jsonObject = null;
        }

        return jsonObject;
    }

    public JSONArray getJSONArrayOutput(){

        String JSONString = getJSONData();
        JSONArray jsonArray = null;

        Log.d("Stocks","The stocks are: "+ JSONString);

        /**
         * Convert the JSON received into a JSONArray
         */
        try {
            jsonArray = new JSONArray(JSONString);
        }
        catch (JSONException e){
            e.printStackTrace();
            jsonArray = null;
        }

        return jsonArray;

    }

    public JSONArray getJSONArrayAtObjectKey(JSONObject json,String key){

        String keyString;
        JSONArray keyJSONArray = null;

        try{
            keyString = json.getString(key);
            keyJSONArray = new JSONArray(keyString);
        }
        catch(JSONException e){
            e.printStackTrace();
        }

        return keyJSONArray;
    }


}
