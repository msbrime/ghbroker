package com.kpakpakpa.ghbroker.data;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.kpakpakpa.ghbroker.GhBrokerOpenHelper;
import com.kpakpakpa.ghbroker.NotificationActivity;
import com.kpakpakpa.ghbroker.models.BrokerNotification;
import com.kpakpakpa.ghbroker.utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Salis on 5/21/2016.
 */
public class UpdateService extends IntentService {

    public static final String SERVICE_TAG = "UpdateService";
    public static final String BROADCAST_ACTION = "com.kpakpakpa.updatelist";
    private static int POLL_INTERVAL = 1000 * 30 * 15;
    private String stocksUrl = "http://dev.kwayisi.org/apis/gse/live";
    private String currencyUrl ="http://broker.tekhabit.com/change.php";

    public UpdateService() {
        super(SERVICE_TAG);
        setIntentRedelivery(true);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        boolean network = Utilities.NetworkChecker.isNetworkAvailable(getApplicationContext());

        if(!network){
            return;
        }

        JSONArray stockData = getStockData();
        JSONArray currencyData = getCurrencyData();

        if(stockData != null){
            updateStocks(stockData);
        }

        if(currencyData != null) {
            updateCurrency(currencyData);
        }

    }

    public static void startUpdateService(Context context){

        Intent intent = new Intent(context,UpdateService.class);
        PendingIntent pi = PendingIntent.getService(context,0,intent,0);

        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        alarm.setRepeating(AlarmManager.RTC,System.currentTimeMillis(),POLL_INTERVAL,pi);

    }

    private void updateCurrency(JSONArray data){

        String updateMessage = "";

        ContentResolver resolver =   getApplicationContext().getContentResolver();

        for(int i = 0;i < data.length();i++){

            try {

                JSONObject json = data.getJSONObject(i);

                String rateName = json.getString("pair").replace("GHS","").replace("/","");

                Double rateDouble = json.getDouble("rate");

                Double changeDouble = json.getDouble("change");

                ContentValues rateValue = new ContentValues();

                rateValue.put("RATE",rateDouble);
                rateValue.put("CHANGE",changeDouble);

                String query = "SELECT a._id,a.ACRONYM,a.TRACKED,b.RATE FROM CURRENCY as a JOIN RATES as b"+
                        " ON a.ACRONYM = b.ACRONYM WHERE a.ACRONYM = '"+rateName+"' AND TRACKED = 1";

                Cursor isTracked = GhBrokerOpenHelper.getInstance(getApplicationContext())
                        .getReadableDatabase()
                        .rawQuery(query,null);

                if(isTracked != null && isTracked.getCount() > 0){

                    isTracked.moveToFirst();
                    int currencyId = isTracked.getInt(isTracked.getColumnIndex("_id"));
                    double oldChange = isTracked.getDouble(isTracked.getColumnIndex("RATE"));

                    double realChange = oldChange - rateDouble;

                    realChange = Double.valueOf(String.format("%.2f",realChange));

                    if(realChange != 0.00) {


                        // inverted because change is computed on the fly
                        String direction = (realChange > 0.00) ? "up" : "down";
                        String realChangeString = String.format("%.2f", realChange).replace("-","");
                        String currencyMsg = rateName +" "+ direction +" "+ realChangeString + ",";

                        updateMessage +=  currencyMsg;

                        logNotification("Currency",currencyMsg.substring(0,(currencyMsg.length()-1)));

                   }

                }

                isTracked.close();
                resolver.update(BrokerContract.RateEntry.CONTENT_URI,rateValue,
                        "ACRONYM = ?",new String[]{rateName});

            }
            catch(JSONException e){
                e.printStackTrace();
            }

        }

        if(!updateMessage.equals("")) {

            updateMessage = updateMessage.substring(0,updateMessage.length()-1);

            BrokerNotification notify =
                    new BrokerNotification(this, updateMessage, updateMessage,
                            NotificationActivity.class);

            notify.setTitle(BrokerNotification.NOTIFY_CURRENCY_TITLE);
            notify.getIntent().addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            notify.getIntent().putExtra(ViewPagerActivityFragment.FROM_NOTIFICATION,1);
//            notify.getIntent().putExtra(ViewPagerActivityFragment.SHOW_TAB,0);
            notify.createPendingIntent();

            BrokerNotification.push(notify);
        }


    }

    private void updateStocks(JSONArray data){

        String updateMessage = "";

        for(int i = 0;i < data.length();i++){
            try {

                JSONObject json = data.getJSONObject(i);

                String rateName = json.getString("name");

                Double priceDouble = json.getDouble("price");

                Double changeDouble = json.getDouble("change");

                ContentValues rateValue = new ContentValues();

                rateValue.put("PRICE",priceDouble);
                rateValue.put("CHANGE",changeDouble);

                String query = "SELECT a._id,a.ACRONYM,a.TRACKED,b.PRICE FROM STOCKS as a JOIN " +
                        "STOCK_RATES as b"+
                        " ON a.ACRONYM = b.ACRONYM WHERE a.ACRONYM = '"+rateName+"' AND TRACKED = 1";

                Cursor isTracked = GhBrokerOpenHelper.getInstance(getApplicationContext())
                        .getReadableDatabase()
                        .rawQuery(query,null);

                if(isTracked != null && isTracked.getCount() > 0){

                    isTracked.moveToFirst();
                    int stockId = isTracked.getInt(isTracked.getColumnIndex("_id"));

                    double oldChange = isTracked.getDouble(isTracked.getColumnIndex("PRICE"));

                    double realChange = oldChange - priceDouble;

                    realChange = Double.valueOf(String.format("%.2f",realChange));

                    if(realChange != 0.00) {

                        // right way up because change has already been computed
                        String direction = (realChange > 0.00) ? "down" : "up";
                        String realChangeString = String.format("%.2f", realChange).replace("-","");
                        String stockMsg = rateName +" "+ direction +" "+ realChangeString + ",";

                        updateMessage += stockMsg;

                        logNotification("Stock",stockMsg.substring(0,(stockMsg.length()-1)));

                    }

                }

                isTracked.close();
                getApplicationContext()
                        .getContentResolver()
                        .update(BrokerContract.StockRateEntry.CONTENT_URI,
                                rateValue,"ACRONYM = ?",new String[]{rateName});
            }
            catch(JSONException e){
                e.printStackTrace();
            }
        }

        if(!updateMessage.equals("")) {

            updateMessage = updateMessage.substring(0,updateMessage.length()-1);

            BrokerNotification notify =
                    new BrokerNotification(this, updateMessage, updateMessage,
                            NotificationActivity.class);

            notify.setTitle(BrokerNotification.NOTIFY_STOCK_TITLE);
            notify.getIntent().addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            notify.getIntent().putExtra(ViewPagerActivityFragment.FROM_NOTIFICATION,1);
//            notify.getIntent().putExtra(ViewPagerActivityFragment.SHOW_TAB,1);
            notify.createPendingIntent();

            BrokerNotification.push(notify);
        }

    }

    private JSONArray getStockData(){

        JSONArray returnData = null;

        try {
            ApiConnector apiconnector = new ApiConnector(stocksUrl);
            returnData = apiconnector.getJSONArrayOutput();
            Log.d("getStockdata","Got the stock data from the service");
        }
        catch(MalformedURLException e){
            e.printStackTrace();
            Log.e("Rate Update Error","Something went wrong updating the stocks");
        }

        return returnData;
    }

    private JSONArray getCurrencyData(){

        JSONArray returnData = null;

        try {

            ApiConnector apiconnector = new ApiConnector(currencyUrl);
            returnData = apiconnector.getJSONArrayOutput();

        }
        catch(MalformedURLException e){
            e.printStackTrace();
            Log.e("Rate Update Error","The url for the rate is wrong");
        }

        return returnData;

    }

    private void logNotification(String type,String message){

        //content values for the date entry
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat tdf = new SimpleDateFormat("h:mm a");
        ContentValues cv = new ContentValues();

        cv.put(BrokerContract.NotificationEntry.COLUMN_TYPE,type);
        cv.put(BrokerContract.NotificationEntry.COLUMN_CONTENT,message);
        cv.put(BrokerContract.NotificationEntry.COLUMN_DATE,df.format(date));
        cv.put(BrokerContract.NotificationEntry.COLUMN_TIME,tdf.format(date));

         getApplicationContext()
                    .getContentResolver()
                    .insert(BrokerContract.NotificationEntry.CONTENT_URI,cv);

    }


}
