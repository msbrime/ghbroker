package com.kpakpakpa.ghbroker.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Salis on 5/22/2016.
 */
public class BrokerContract {

    public static final String CONTENT_AUTHORITY = "com.kpakpakpa.ghbroker.app";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_CURRENCY = "CURRENCY";
    public static final String PATH_CURRENCY_RATE = "RATES";
    public static final String PATH_STOCK = "STOCKS";
    public static final String PATH_STOCK_RATE = "STOCK_RATES";
    public static final String PATH_NOTIFICATIONS = "NOTIFICATIONS";

    public static final class CurrencyEntry implements BaseColumns{

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CURRENCY).build();

        public static final String CONTENT_TYPE =
                    "vnd.android.cursor.dir/"+CONTENT_AUTHORITY+ "/" + PATH_CURRENCY;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"+CONTENT_AUTHORITY+ "/" + PATH_CURRENCY;

        //the name of the table
        public static final String TABLE_NAME = "CURRENCY";

        //the name of the currency
        public static final String COLUMN_NAME = "NAME";

        //the acronym of the currency
        public static final String COLUMN_ACRONYM = "ACRONYM";

        //the acronym of the currency
        public static final String COLUMN_TRACKED = "TRACKED";

        //the image_res_id of the corresponding image,saved as integer
        public static final String COLUMN_IMAGE_RES_ID = "IMAGE_RES_ID";

        //the exchange rate per unit,saved as flaot
        public static final String COLUMN_RATE = "RATE";

        //the change in rate,saved as float
        public static final String COLUMN_CHANGE = "CHANGE";

        public static Uri buildStockUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }

    }

    public static final class StockEntry implements BaseColumns{

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_STOCK).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/"+CONTENT_AUTHORITY+ "/" + PATH_STOCK;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"+CONTENT_AUTHORITY+ "/" + PATH_STOCK;


        //the name of the table
        public static final String TABLE_NAME = "STOCKS";

        //the name of the currency
        public static final String COLUMN_NAME = "NAME";

        //the acronym of the currency
        public static final String COLUMN_ACRONYM = "ACRONYM";

        //the acronym of the currency
        public static final String COLUMN_TRACKED = "TRACKED";

        //the image_res_id of the corresponding image,saved as integer
        public static final String COLUMN_IMAGE_RES_ID = "IMAGE_RES_ID";

        //the price per share,saved as flaot
        public static final String COLUMN_PRICE = "PRICE";

        //the change in share price,saved as float
        public static final String COLUMN_CHANGE = "CHANGE";


        public static Uri buildStockUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }

    }

    public static class StockRateEntry implements BaseColumns{


        public static final Uri CONTENT_URI  =
                  BASE_CONTENT_URI.buildUpon().appendPath(PATH_STOCK_RATE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_STOCK_RATE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_STOCK_RATE;

        //the name of the table
        public static final String TABLE_NAME = "STOCK_RATES";

        //the acronym of the currency
        public static final String COLUMN_ACRONYM = "ACRONYM";

        //the price per share,saved as flaot
        public static final String COLUMN_PRICE = "PRICE";

        //the change in rate,saved as float
        public static final String COLUMN_CHANGE = "CHANGE";


    }


    public static class RateEntry implements BaseColumns{

        public static final Uri CONTENT_URI  =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CURRENCY_RATE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_CURRENCY_RATE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_CURRENCY_RATE;

        //the name of the table
        public static final String TABLE_NAME = "RATES";

        //the acronym of the currency
        public static final String COLUMN_ACRONYM = "ACRONYM";

        //the exchange rate per unit,saved as flaot
        public static final String COLUMN_RATE = "RATE";

        //the change in rate,saved as float
        public static final String COLUMN_CHANGE = "CHANGE";


    }

    public static class NotificationEntry implements BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_NOTIFICATIONS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_NOTIFICATIONS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_NOTIFICATIONS;

        public static final String TABLE_NAME = "NOTIFICATIONS";

        //the type of item the notification is for
        public static final String COLUMN_TYPE = "TYPE";

        //the content of the notification
        public static final String COLUMN_CONTENT = "CONTENT";

        //the time which the notification was posted
        public static final String COLUMN_TIME = "TIME";


        public static final String COLUMN_DATE = "DATE";

    }

}
