package com.kpakpakpa.ghbroker.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.kpakpakpa.ghbroker.GhBrokerOpenHelper;

/**
 * Created by Salis on 5/22/2016.
 */
public class BrokerProvider extends ContentProvider {

    private static final int CURRENCY = 100;
    private static final int CURRENCY_ID = 101;
    private static final int CURRENCY_RATE = 102;
    private static final int CURRENCY_RATE_ENTRY = 103;
    private static final int STOCK = 200;
    private static final int STOCK_ID = 201;
    private static final int STOCK_RATE = 202;
    private static final int STOCK_RATE_ENTRY = 203;
    private static final int NOTIFICATION = 300;
    private static final int NOTIFICATION_ENTRY = 301;

    private static final UriMatcher pbUriMatcher = buildUriMatcher();

    private SQLiteOpenHelper brokerOpenHelper;

    @Override
    public boolean onCreate() {
        brokerOpenHelper = GhBrokerOpenHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor retCursor;

        switch(pbUriMatcher.match(uri)){
            case CURRENCY:
                String curr_query =  "SELECT a._id,a.ACRONYM,a.NAME,a.IMAGE_RES_ID,a.TRACKED,b.RATE,b.CHANGE " +
                                "FROM CURRENCY AS a JOIN RATES AS b ON " +
                                "a.ACRONYM = b.ACRONYM WHERE a.ACRONYM NOT IN('GHS')";

                retCursor = brokerOpenHelper.getReadableDatabase().rawQuery(curr_query,null);
                break;
            case STOCK:
                String stock_query = "SELECT a._id,a.ACRONYM,a.NAME,a.IMAGE_RES_ID,a.TRACKED,b.PRICE,b.CHANGE " +
                        "FROM STOCKS AS a JOIN STOCK_RATES AS b ON " +
                        "a.ACRONYM = b.ACRONYM";
                retCursor = brokerOpenHelper.getReadableDatabase().rawQuery(stock_query,null);
                break;
            case CURRENCY_RATE:
                String currencyRateQuery = "SELECT * FROM RATES WHERE ACRONYM NOT IN('GHS')";
                retCursor = brokerOpenHelper.getReadableDatabase().rawQuery(currencyRateQuery,null);
                break;
            case CURRENCY_ID:

                    retCursor = brokerOpenHelper.getReadableDatabase()
                                                .query(BrokerContract.CurrencyEntry.TABLE_NAME,
                                                       projection,selection,selectionArgs,null,null,null);
                    break;
            case NOTIFICATION:
                retCursor = brokerOpenHelper.getReadableDatabase()
                        .query(BrokerContract.NotificationEntry.TABLE_NAME,
                                projection,selection,selectionArgs,null,null,sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Uri not recognized"+ uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(),uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        final int match = pbUriMatcher.match(uri);

        switch(match){
            case CURRENCY:
                return BrokerContract.CurrencyEntry.CONTENT_TYPE;
            case CURRENCY_ID:
                return BrokerContract.CurrencyEntry.CONTENT_ITEM_TYPE;
            case STOCK:
                return BrokerContract.StockEntry.CONTENT_TYPE;
            case STOCK_ID:
                return BrokerContract.StockEntry.CONTENT_ITEM_TYPE;
            case CURRENCY_RATE:
                return BrokerContract.RateEntry.CONTENT_TYPE;
            case STOCK_RATE:
                return BrokerContract.StockRateEntry.CONTENT_TYPE;
            case CURRENCY_RATE_ENTRY:
                return BrokerContract.RateEntry.CONTENT_ITEM_TYPE;
            case STOCK_RATE_ENTRY:
                return BrokerContract.StockRateEntry.CONTENT_ITEM_TYPE;
            case NOTIFICATION:
                return BrokerContract.NotificationEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown Uri");
        }

    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        final SQLiteDatabase db = brokerOpenHelper.getWritableDatabase();
        final int match = pbUriMatcher.match(uri);
        final long insertId;
        Uri returnUri = null;

        switch(match){
            case NOTIFICATION:
                insertId = db.insert(BrokerContract.NotificationEntry.TABLE_NAME,null,values);
                if(insertId > 0){
                    returnUri = ContentUris.withAppendedId(BrokerContract.NotificationEntry.CONTENT_URI,insertId);
                    getContext().getContentResolver().notifyChange(uri,null);
                }
                else{
                    throw new android.database.SQLException("Failed to insert row into "+ uri);
                }
                break;
        }

        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        final SQLiteDatabase db = brokerOpenHelper.getWritableDatabase();
        final int match = pbUriMatcher.match(uri);
        final int rowsUpdated;

        switch(match){
            case NOTIFICATION:
                rowsUpdated = db.delete(BrokerContract.NotificationEntry.TABLE_NAME,selection,selectionArgs);
                if(rowsUpdated > 0){
                    getContext().getContentResolver().notifyChange(uri,null);
                    return rowsUpdated;
                }
                else{
                    throw new android.database.SQLException("Something went wrong "+ uri);
                }
        }

        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = brokerOpenHelper.getWritableDatabase();
        final int match = pbUriMatcher.match(uri);
        final int rowsUpdated;
        final Uri notifyUri;

        switch(match){
            case STOCK_RATE:
                rowsUpdated = db.update(BrokerContract.StockRateEntry.TABLE_NAME,values,selection,selectionArgs);
                notifyUri = BrokerContract.StockEntry.CONTENT_URI;
                break;
            case CURRENCY_RATE:
                rowsUpdated = db.update(BrokerContract.RateEntry.TABLE_NAME,values,selection,selectionArgs);
                notifyUri = BrokerContract.CurrencyEntry.CONTENT_URI;
                break;
            case CURRENCY:
                rowsUpdated = db.update(BrokerContract.CurrencyEntry.TABLE_NAME,values,selection,selectionArgs);
                notifyUri = BrokerContract.CurrencyEntry.CONTENT_URI;
                break;
            case STOCK:
                rowsUpdated = db.update(BrokerContract.StockEntry.TABLE_NAME,values,selection,selectionArgs);
                notifyUri = BrokerContract.CurrencyEntry.CONTENT_URI;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported URI: " + uri);
        }

        if(rowsUpdated != 0){
            getContext().getContentResolver().notifyChange(notifyUri,null);
            getContext().getContentResolver().notifyChange(uri,null);
        }

        return rowsUpdated;

    }

    private static UriMatcher buildUriMatcher(){

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = BrokerContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, BrokerContract.PATH_CURRENCY,CURRENCY);
        matcher.addURI(authority,BrokerContract.PATH_CURRENCY + "/*", CURRENCY_ID);
        matcher.addURI(authority, BrokerContract.PATH_STOCK,STOCK);
        matcher.addURI(authority,BrokerContract.PATH_STOCK+ "/#", STOCK_ID);
        matcher.addURI(authority,BrokerContract.PATH_CURRENCY_RATE + "/*", CURRENCY_RATE_ENTRY);
        matcher.addURI(authority,BrokerContract.PATH_STOCK_RATE + "/*", STOCK_RATE_ENTRY );
        matcher.addURI(authority,BrokerContract.PATH_CURRENCY_RATE , CURRENCY_RATE);
        matcher.addURI(authority,BrokerContract.PATH_STOCK_RATE , STOCK_RATE );
        matcher.addURI(authority,BrokerContract.PATH_NOTIFICATIONS , NOTIFICATION );
        matcher.addURI(authority,BrokerContract.PATH_NOTIFICATIONS + "/#" , NOTIFICATION_ENTRY );

        return matcher;
    }

}
