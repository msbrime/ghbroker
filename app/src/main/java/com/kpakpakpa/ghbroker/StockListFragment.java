package com.kpakpakpa.ghbroker;


import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kpakpakpa.ghbroker.data.BrokerContract;
import com.kpakpakpa.ghbroker.models.Currency;
import com.kpakpakpa.ghbroker.models.CurrencyUpdate;
import com.kpakpakpa.ghbroker.models.DoubleParts;
import com.kpakpakpa.ghbroker.models.Stock;
import com.kpakpakpa.ghbroker.utility.ItemUpdateInterface;

import java.text.DecimalFormat;


/**
 * A simple {@link Fragment} subclass.
 */
public class StockListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,ItemUpdateInterface {

    private View parentView;
    private ListView stockList;
    private CursorAdapter CurrencyAdapter;
    private static final int STOCK_LOADER = 0;

    private static final String[] STOCK_COLUMNS ={
            BrokerContract.StockEntry.TABLE_NAME+"."+BrokerContract.StockEntry._ID,
            BrokerContract.StockEntry.COLUMN_ACRONYM,
            BrokerContract.StockEntry.COLUMN_NAME,
            BrokerContract.StockEntry.COLUMN_IMAGE_RES_ID,
            BrokerContract.StockEntry.COLUMN_TRACKED,
            BrokerContract.StockEntry.COLUMN_PRICE,
            BrokerContract.StockEntry.COLUMN_CHANGE
    };

    public StockListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        parentView = inflater.inflate(R.layout.fragment_currency_list, container, false);

        return parentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        CurrencyAdapter =
                new CustomCursorAdapter(getActivity(), R.layout.stock_row,
                        null, new String[]{"IMAGE_RES_ID", "PRICE"},
                        new int[]{R.id.stock_image, R.id.stock_rate});

        stockList = (ListView) parentView.findViewById(R.id.stock_list);

        stockList.setAdapter(CurrencyAdapter);

        getLoaderManager().initLoader(STOCK_LOADER,null,this);
        registerForContextMenu(stockList);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void updateComplete(int success) {

    }

    /**
     * showTrackDialog function
     *
     * function that shows a dialog with the track status
     * of a currency and gives options to keep tracking or stop
     *
     * @param currency
     * @param context
     *
     * @return void
     */
    public void showTrackDialog(final Currency currency, Context context){
        final Dialog dialog = new Dialog(context);

        String titleMessage = (currency.getTrackStatus() > 0) ? "Untrack" : "Track";
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);

        TextView dialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        dialogTitle.setText(titleMessage + " " + currency.getName());

        Button track_status = (Button) dialog.findViewById(R.id.track_status);
        Button dismissDialog = (Button) dialog.findViewById(R.id.dismiss);

        track_status.setText(titleMessage);

        /**
         * click listener for the track button
         */
        track_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int track = (currency.getTrackStatus() > 0) ? 0 : 1;
                ContentValues cv = new ContentValues();
                cv.put("TRACKED",track);

                CurrencyUpdate currencyUpdate =
                        new CurrencyUpdate(getActivity(),cv,"ACRONYM = ?",
                                new String[]{currency.getAcronym()});

                new Stock.UpdateStockTask().execute(currencyUpdate);

                dialog.dismiss();
            }
        });

        /**
         * define click listener for the cancel button
         */
        dismissDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        //Toast.makeText(context,cursor.getString( cursor.getColumnIndex("NAME") ),Toast.LENGTH_LONG).show();
    }

    /**
     * Custom cursor adapter class that handles the parsing of the
     * results from the currency db
     */
    private class CustomCursorAdapter extends SimpleCursorAdapter {

        private LayoutInflater cursorInflater;

        CustomCursorAdapter(Context context,int layout,Cursor cursor,String[] from,int[] to){
            super(context,layout,cursor,from,to);
            cursorInflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return cursorInflater.inflate(R.layout.stock_row, parent, false);
        }

        @Override
        public void bindView(View view,final Context context, Cursor cursor) {

            TextView textViewTitle = (TextView) view.findViewById(R.id.stock_acr);
            ImageView stockImage = (ImageView) view.findViewById(R.id.stock_image);
            ImageView trackIcon = (ImageView) view.findViewById(R.id.tracked_icon);
            TextView textViewRate = (TextView) view.findViewById(R.id.stock_rate);
            TextView textViewRateDec = (TextView) view.findViewById(R.id.stock_rate_decimal);
            TextView textViewChange = (TextView) view.findViewById(R.id.stock_change);
            TextView textViewChangeDec = (TextView) view.findViewById(R.id.stock_change_decimal);


            String acr = cursor.getString( cursor.getColumnIndex("ACRONYM") );
            final String name = cursor.getString( cursor.getColumnIndex("NAME") );
            Double change = cursor.getDouble(cursor.getColumnIndex("CHANGE"));
            Double rate = cursor.getDouble(cursor.getColumnIndex("PRICE"));
            int imageRes = cursor.getInt( cursor.getColumnIndex("IMAGE_RES_ID") );
            final int id = cursor.getInt( cursor.getColumnIndex("_id") );
            final int trackStatus = cursor.getInt(cursor.getColumnIndex("TRACKED"));

            final Stock stock= new Stock(name,acr,imageRes,trackStatus);

            if(trackStatus > 0){
                trackIcon.setVisibility(View.VISIBLE);
                trackIcon.setImageResource(R.drawable.track);
            }
            else{
                trackIcon.setVisibility(View.GONE);
            }

            DecimalFormat df = new DecimalFormat("#0.00");

            String title = acr;

            DoubleParts rateParts = new DoubleParts(rate);

            DoubleParts changeParts = new DoubleParts(change);

            stockImage.setImageResource(imageRes);

            textViewTitle.setText(title);

            textViewRate.setText(rateParts.integerPartAsString);

            textViewRateDec.setText(rateParts.decimalPartAsString);

            textViewChange.setText(changeParts.integerPartAsString);

            textViewChangeDec.setText(changeParts.decimalPartAsString);

            if(Double.parseDouble(df.format(change)) < 0.00){
                textViewChange.setTextColor(getResources().getColor(R.color.colorRed));
                textViewChangeDec.setTextColor(getResources().getColor(R.color.colorRed));
            }

            if(Double.parseDouble(df.format(change)) > 0.00){

                textViewChange.setTextColor(getResources().getColor(R.color.colorGreen));
                textViewChangeDec.setTextColor(getResources().getColor(R.color.colorGreen));

            }

            if(Double.parseDouble(df.format(change)) == 0.00){

                textViewChange.setTextColor(getResources().getColor(R.color.colorPrimaryText));
                textViewChangeDec.setTextColor(getResources().getColor(R.color.colorPrimaryText));

            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getActivity(), DetailsActivity.class);
                    intent.putExtra(DetailsActivity.ITEM_ID,  id);
                    intent.putExtra(DetailsActivity.ITEM_TYPE,"stock");
                   startActivity(intent);
                }
            });

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    showTrackDialog(stock,context);
                    return false;
                }
            });
        }
    }
    /** End of CustomCursorAdapter **/

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),BrokerContract.StockEntry.CONTENT_URI,STOCK_COLUMNS,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        CurrencyAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        CurrencyAdapter.swapCursor(null);
    }
}
