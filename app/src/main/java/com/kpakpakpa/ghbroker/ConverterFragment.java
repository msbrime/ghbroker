package com.kpakpakpa.ghbroker;


import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.kpakpakpa.ghbroker.data.BrokerContract;
import com.kpakpakpa.ghbroker.models.Currency;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConverterFragment extends Fragment {

    private static final int DEFAULT_IMG_RSRC = R.drawable.cedi;

    private View parentView;
    private ArrayList<Currency> rates = new ArrayList<Currency>();
    private double conversionRate;

    private boolean toCedis = true;

    private EditText convertFrom;
    private EditText convertTo;

    private ImageButton toCedisButton;

    private ImageView convertFromImage;
    private ImageView convertToImage;

    private Map<Integer,Integer> image_map = new HashMap<Integer,Integer>();


    public ConverterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_converter, container, false);

        convertFrom = (EditText) parentView.findViewById(R.id.convert_from);
        convertTo = (EditText) parentView.findViewById(R.id.convert_to);

        convertFromImage = (ImageView) parentView.findViewById(R.id.convert_from_image);
        convertToImage = (ImageView) parentView.findViewById(R.id.convert_to_image);


        toCedisButton = (ImageButton) parentView.findViewById(R.id.to_cedis_button);

        toCedisButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                toCedis = !toCedis;
                if(!toCedis){
                    convertFromImage.setTag(R.id.current_image_rsrc,(int) convertToImage.getTag(R.id.current_image_rsrc));
                    convertFromImage.setImageResource((int) convertToImage.getTag(R.id.current_image_rsrc));
                    convertToImage.setImageResource(DEFAULT_IMG_RSRC);
                }
                else{
                    convertToImage.setTag(R.id.current_image_rsrc,(int) convertFromImage.getTag(R.id.current_image_rsrc));
                    convertToImage.setImageResource((int) convertFromImage.getTag(R.id.current_image_rsrc));
                    convertFromImage.setImageResource(DEFAULT_IMG_RSRC);
                }

                convertFrom.setText(convertFrom.getText());
                convertFrom.setSelection(convertFrom.getText().length());
            }
        });

        image_map.put(R.drawable.dollar,R.drawable.dollar);
        image_map.put(R.drawable.pound,R.drawable.pound);
        image_map.put(R.drawable.euro,R.drawable.euro);
        image_map.put(R.drawable.yen,R.drawable.yen);
        image_map.put(R.drawable.sfranc,R.drawable.sfranc);
        image_map.put(R.drawable.naira,R.drawable.naira);
        image_map.put(R.drawable.australian,R.drawable.australian);
        image_map.put(R.drawable.canadian,R.drawable.canadian);
        image_map.put(R.drawable.yuan,R.drawable.yuan);
        image_map.put(R.drawable.rupee,R.drawable.rupee);
        image_map.put(R.drawable.rand,R.drawable.rand);

        convertFrom.addTextChangedListener(conversionWatcher);

        return parentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        new FetchCurrencyRatesTask().execute();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu,menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        try{
            menu.getItem(1);
        }
        catch(IndexOutOfBoundsException e){

            for(Currency r: rates){

                menu.add(Menu.NONE,r.getId(),Menu.NONE,r.getName());

            }

            try {
                setConversionRate(0);
                convertToImage.setTag(R.id.current_image_rsrc, rates.get(0).getImage());
                convertToImage.setImageResource(rates.get(0).getImage());
            }
            catch(IndexOutOfBoundsException ex){
                ex.printStackTrace();
            }

        }

            super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        setConversionRate(item.getItemId());

        Currency currency = rates.get(item.getItemId());

        if(toCedis){
            convertToImage.setTag(R.id.current_image_rsrc,currency.getImage());
            convertToImage.setImageResource(currency.getImage());
        }
        else{
            convertFromImage.setTag(R.id.current_image_rsrc,currency.getImage());
            convertFromImage.setImageResource(currency.getImage());
        }

        convertFrom.setText(convertFrom.getText());
        convertFrom.setSelection(convertFrom.getText().length());

        return true;

    }

    protected void setConversionRate(int i){

        try {
            conversionRate = rates.get(i).getRate();
        }
        catch(IndexOutOfBoundsException e){
            e.printStackTrace();
        }

    }

    protected double convert(double value,boolean convertTo){

        if(convertTo) {
            return value / conversionRate;
        }

        return value * conversionRate;
    }


    protected TextWatcher conversionWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {


        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String convertValue = convertFrom.getText().toString();

            convertValue = (convertValue.length() > 0) ? convertValue : "0";

            double value = Double.parseDouble(convertValue);

            value = Double.valueOf(String.format("%.2f",convert(value,toCedis)));

            convertTo.setText(NumberFormat.getNumberInstance(Locale.US).format(value));

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private class FetchCurrencyRatesTask extends AsyncTask<Void,Void,Cursor>{

        @Override
        protected Cursor doInBackground(Void... params) {
            return getActivity().getContentResolver().query(BrokerContract.CurrencyEntry.CONTENT_URI,null,null,null,null);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if(cursor != null || cursor.getCount() > 0){

                cursor.moveToFirst();
                int monitor = 0;

                do{

                    double rate = cursor.getDouble(cursor.getColumnIndex("RATE"));
                    String name = cursor.getString(cursor.getColumnIndex("NAME"));
                    int image = cursor.getInt(cursor.getColumnIndex("IMAGE_RES_ID"));
                    String acronym = cursor.getString(cursor.getColumnIndex("ACRONYM"));

                    rates.add(new Currency(name,acronym,image_map.get(image),monitor,rate));

                    monitor++;

                }while(cursor.moveToNext());

                cursor.close();

                ActivityCompat.invalidateOptionsMenu(getActivity());

            }
            else{
                new FetchCurrencyRatesTask().execute();
            }
        }
    }




}
