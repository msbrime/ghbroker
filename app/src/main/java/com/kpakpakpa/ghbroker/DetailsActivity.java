package com.kpakpakpa.ghbroker;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.kpakpakpa.ghbroker.data.ApiConnector;
import com.kpakpakpa.ghbroker.models.Company;
import com.kpakpakpa.ghbroker.models.Currency;
import com.kpakpakpa.ghbroker.models.CurrencyUpdate;
import com.kpakpakpa.ghbroker.models.Stock;
import com.kpakpakpa.ghbroker.utility.BrokerValueFormatter;
import com.kpakpakpa.ghbroker.utility.ItemUpdateInterface;
import com.kpakpakpa.ghbroker.utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity implements ItemUpdateInterface {

    public static final String ITEM_ID = "stockId";
    public static final String ITEM_TYPE = "itemType";
    private int isTracked;
    private Cursor cursor = null;
    private SQLiteDatabase db = null;
    private String itemType;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private LineChart chart;
    private StockDatabaseTask stockTask = null;
    private Currency brokerItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onNewIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        int stockId = intent.getIntExtra(ITEM_ID,1);
        itemType = intent.getStringExtra(ITEM_TYPE);

        switch(itemType){
            case "stock":
                setContentView(R.layout.activity_details_stock);
                break;
            default:
                setContentView(R.layout.activity_details_currency);
                chart = (LineChart) findViewById(R.id.chart);
                chart.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        stockTask = new StockDatabaseTask();

        stockTask.execute(stockId);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(stockTask != null){
            stockTask.cancel(true);
        }
    }

    @Override
    public void updateComplete(int success) {

        if(success == 1){

            isTracked = (isTracked == 0) ? 1 : 0;
            String message = (isTracked == 1) ? "Now Tracking " : "Stopped Tracking ";
            message += brokerItem.getAcronym();

           Snackbar.make(findViewById(R.id.details_root),message,Snackbar.LENGTH_SHORT).show();
            invalidateOptionsMenu();
        }
    }

    private class StockDatabaseTask extends AsyncTask<Integer,Void,Cursor> {

        @Override
        protected Cursor doInBackground(Integer... params) {

            int stockId = params[0];

            try {

                SQLiteOpenHelper sqlOpen = GhBrokerOpenHelper.getInstance(DetailsActivity.this);

                String tableName;

                db = sqlOpen.getReadableDatabase();

                switch(itemType){
                    case "stock":
                        tableName = "STOCKS";
                        break;
                    default:
                        tableName = "CURRENCY";
                }

                cursor = db.query(tableName,
                            new String[]{"_id","NAME", "ACRONYM", "IMAGE_RES_ID","TRACKED"},
                            "_id = ?",
                            new String[]{Integer.toString(stockId)},
                            null, null, null);

                return cursor;

            }
            catch(SQLiteException e){
                e.printStackTrace();
            }

            return cursor;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if(cursor != null ){

                cursor.moveToFirst();

                String acronym = cursor.getString(cursor.getColumnIndex("ACRONYM"));
                String title = cursor.getString(cursor.getColumnIndex("NAME"));
                int image = cursor.getInt(cursor.getColumnIndex("IMAGE_RES_ID"));
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                isTracked = cursor.getInt(cursor.getColumnIndex("TRACKED"));

                brokerItem = new Currency(title,acronym,image,isTracked);

                ActionBar ctl = getSupportActionBar();

                ctl.setTitle(title);

                TextView acr = (TextView) findViewById(R.id.acronym);
                TextView name = (TextView) findViewById(R.id.name);

                invalidateOptionsMenu();

                acr.setText(acr.getText()+acronym);
                name.setText(name.getText()+title);

                boolean network = Utilities.NetworkChecker.isNetworkAvailable(DetailsActivity.this);

                if(network) {

                    if(DetailsActivity.this.itemType.equals("currency")) {
                        new getCurrencyHistoryTask().execute(acronym);
                    }
                    else{
                        new getCompanyInfoTask().execute(acronym);
                    }
                }
                else{

                    Toast.makeText(DetailsActivity.this,"There is no internet",Toast.LENGTH_SHORT).show();
                }

            }

        }
    }


    private class getCompanyInfoTask extends AsyncTask<String,Void,JSONObject>{
        @Override
        protected JSONObject doInBackground(String... params) {

            JSONObject returnString = null;

            try {
                ApiConnector connector = new ApiConnector("http://dev.kwayisi.org/apis/gse/equities/" + params[0].toLowerCase());

                returnString = connector.getJSONObjectOutput();
            }
            catch(MalformedURLException e){
                e.printStackTrace();
            }

            return returnString;

        }

        @Override
        protected void onPostExecute(JSONObject j) {
            super.onPostExecute(j);
            if(j != null){

                Company company = new Company(j);

                TextView textViewDps = (TextView) findViewById(R.id.dps);
                TextView textViewEps = (TextView) findViewById(R.id.eps);
                TextView textViewShares = (TextView) findViewById(R.id.shares);
                TextView textViewPrice = (TextView) findViewById(R.id.price);
                TextView textViewSector = (TextView) findViewById(R.id.sector);
                TextView textViewIndustry = (TextView) findViewById(R.id.industry);
                TextView textViewFacsimile = (TextView) findViewById(R.id.facsimile);
                TextView textViewEmail = (TextView) findViewById(R.id.email);
                TextView textViewWebsite = (TextView) findViewById(R.id.website);
                TextView textViewTelephone = (TextView) findViewById(R.id.telephone);


                textViewDps.setText(String.format("%.2f",company.dps));
                textViewEps.setText(String.format("%.2f",company.eps));
                textViewShares.setText(String.format("%.2f",company.shares));
                textViewPrice.setText(String.format("%.2f",company.price));
                textViewSector.setText(company.sector);
                textViewIndustry.setText(company.industry);
                textViewFacsimile.setText(company.facsimile);
                textViewEmail.setText(company.email);
                textViewWebsite.setText(company.website);
                textViewTelephone.setText(company.telephone);

            }
        }
    }


    private class getCurrencyHistoryTask extends AsyncTask<String,Void,JSONArray>{

        @Override
        protected JSONArray doInBackground(String... params) {

            JSONArray returnString = null;

            try {
                ApiConnector connector = new ApiConnector("http://broker.tekhabit.com/history.php?curr=" + params[0]);

                returnString = connector.getJSONArrayOutput();
            }
            catch(MalformedURLException e){
                e.printStackTrace();
            }

            return returnString;
        }

        @Override
        protected void onPostExecute(JSONArray s) {
            super.onPostExecute(s);
            if(s == null){
                return;
            }
            try {

                ArrayList<Entry> valsComp1 = new ArrayList<Entry>();
                ArrayList<String> xVals = new ArrayList<String>();

                for (int i = 0; i < s.length(); i++) {

                    JSONObject a = new JSONObject(s.getString(i));

                    double rate = a.getDouble("rate");

                    rate = Double.valueOf(String.format("%.2f",rate));

                    valsComp1.add(new Entry((float) rate, i));
                    xVals.add(a.getString("date"));

                }

                //Disable right side of X axis
                chart.getAxisRight().setDrawGridLines(false);
                chart.getAxisRight().setEnabled(false);


                YAxis yAxisL = chart.getAxisLeft();
                yAxisL.setDrawGridLines(false);

                XAxis xaxis = chart.getXAxis();
                xaxis.setDrawGridLines(false);
                xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);

                xaxis.setLabelRotationAngle(290.00f);
                xaxis.setLabelsToSkip(0);

                LineDataSet setComp1 = new LineDataSet(valsComp1, "Currency");
                setComp1.setAxisDependency(YAxis.AxisDependency.LEFT);
                setComp1.setValueFormatter(new BrokerValueFormatter());

                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                dataSets.add(setComp1);

                LineData data = new LineData(xVals, dataSets);
                chart.setData(data);
                chart.setScaleEnabled(false);

                chart.invalidate(); // refresh

            }catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.activity_details_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.clear();


        MenuItem item = menu.add(Menu.NONE,R.id.action_track,Menu.NONE,"Track");

        if(isTracked == 0){
            item.setIcon(getResources().getDrawable(R.drawable.track_white));

        }else
        {
            item.setIcon(getResources().getDrawable(R.drawable.track_white_cancel));
        }

        MenuItemCompat.setShowAsAction(item,MenuItem.SHOW_AS_ACTION_IF_ROOM);


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int trackUpdate = (isTracked == 1)? 0 : 1;
        ContentValues cv = new ContentValues();
        cv.put("TRACKED",trackUpdate);
        CurrencyUpdate cu = new CurrencyUpdate(this,cv,"ACRONYM = ?",new String[]{brokerItem.getAcronym()});

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_track:

                if(itemType.equals("currency")){
                    new Currency.UpdateCurrencyTask(this).execute(cu);
                }
                else{
                    new Stock.UpdateStockTask(this).execute(cu);
                }
                break;
        }
        return true;
    }
}
